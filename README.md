# telldus-exporter
A simple [Prometheus](http://prometheus.io) exporter of [Telldus](http://telldus.com) sensor data.

## Instructions
Add your Telldus Live API key and token to apikeys.json before launching the script or building the Dockerfile.

## Docker image
There is a docker image built. You should add your own credentials to apikeys.json and start it with:

```sh
docker run -d -p 9191:9191 --restart="always" -v /path/to/apikeys:/etc/telldus-exporter \
            registry.gitlab.com/rlnrln/telldus-exporter:latest
```

Point your web browser to http://YOUR_IP:9191/metrics to verify you're getting data from the Telldus API.
